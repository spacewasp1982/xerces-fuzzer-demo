#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

OUTPUT_FOLDER=/mnt/output
CORPUS_FOLDER=/mnt/corpus

mkdir -p $CORPUS_FOLDER
mkdir -p $OUTPUT_FOLDER

chmod a+rw $CORPUS_FOLDER $OUTPUT_FOLDER

SYNC_FILE=$OUTPUT_FOLDER/jazzer_sync
touch $SYNC_FILE

CLASSPATH=$(find /fuzzer-target -name "*.jar" | tr '\n' ':')

echo "127.0.0.1 $HOSTNAME" >> /etc/hosts

INSTRUMENTATION_INCLUDES=\
com.sun.org.apache.xerces.**:\
org.xml.**

cd $OUTPUT_FOLDER

/build/jazzer_driver \
--cp=$CLASSPATH \
--instrumentation_includes=$INSTRUMENTATION_INCLUDES \
--target_class=$TARGET_CLASS \
--jvm_args=-Xmx4g \
--keep_going=16 \
-seed=42 \
-max_len=4096 \
-rss_limit_mb=8192 \
-use_value_profile=1 \
-close_fd_mask=2 \
--reproducer_path=$OUTPUT_FOLDER \
--id_sync_file=$SYNC_FILE \
-artifact_prefix=$OUTPUT_FOLDER/ \
$CORPUS_FOLDER >> $OUTPUT_FOLDER/$HOSTNAME.log 2>&1

echo 'DONE'
