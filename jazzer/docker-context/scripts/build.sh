#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

wget https://github.com/bazelbuild/bazelisk/releases/download/v1.11.0/bazelisk-linux-amd64
chmod +x bazelisk-linux-amd64

wget https://github.com/CodeIntelligenceTesting/jazzer/archive/refs/tags/v0.11.0.zip
unzip -o v0.11.0.zip

#COMMIT=16b8e6bc5ac3b47e930c0d37b3ec7461ae21ee1e
#wget https://github.com/CodeIntelligenceTesting/jazzer/archive/$COMMIT.zip
#unzip -o $COMMIT.zip
#mv jazzer-$COMMIT jazzer-main

pushd jazzer-0.11.0
    /work/bazelisk-linux-amd64 build --sandbox_debug //:jazzer_asan //:jazzer_ubsan //:jazzer

    cp $(find bazel-out/ -type f -regextype posix-extended \
        -regex ".*/(jazzer_(api|agent)_deploy\.jar|jazzer_driver(_asan|_ubsan)?)") /build
popd


