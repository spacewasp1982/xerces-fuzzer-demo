#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

OUTPUT_FOLDER=/mnt/output
cd $OUTPUT_FOLDER

CLASSPATH=$(find /fuzzer-target -name "*.jar" | tr '\n' ':' )

java -Xmx4g -cp $CLASSPATH $TARGET_CLASS $PAYLOAD
