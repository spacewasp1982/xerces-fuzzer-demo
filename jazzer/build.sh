#!/usr/bin/env bash

# Fail when commands exit unsuccesfully.
set -o errexit

# Fail when using an undefined variable.
set -o nounset

# Fail if commands fail as part of a pipeline.
set -o pipefail


BUILD_DIR=$(pwd)/out

mkdir -p $BUILD_DIR

IMAGE_TAG="jazzer-project"

docker build -t $IMAGE_TAG docker-context

docker run \
    --entrypoint="/work/build.sh" \
    --mount type=bind,source="$BUILD_DIR",target=/build \
    --mount type=bind,source=$(realpath ../openjdk/out),target=/jdk \
    $IMAGE_TAG

#--mount type=bind,source="$BUILD_DIR",target=/build \
#--mount type=bind,source=$(pwd)/corretto-18-develop,target=/corretto-18-develop \

