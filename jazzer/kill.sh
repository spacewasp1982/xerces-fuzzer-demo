#!/usr/bin/env bash

containers=$(docker ps -a -q)

[ -z "$containers" ] && exit 0

docker kill $containers
docker rm $containers
rm -f output/jazzer_sync

