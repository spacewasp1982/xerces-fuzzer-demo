#!/usr/bin/env bash

# Fail when commands exit unsuccesfully.
set -o errexit

# Fail when using an undefined variable.
set -o nounset

# Fail if commands fail as part of a pipeline.
set -o pipefail


BUILD_DIR=$(pwd)/out

mkdir -p $BUILD_DIR

IMAGE_TAG="jazzer-project"

TARGET_CLASS=com.example.JazzerTarget

docker build -t $IMAGE_TAG docker-context

CPU_COUNT=$(grep -c processor /proc/cpuinfo)
for (( i=0; i < (CPU_COUNT / 2); i++)); do
    docker run \
        --detach \
        --network none \
        --entrypoint="/work/fuzz.sh" \
        --mount type=bind,source="$(pwd)",target=/mnt \
        --mount type=bind,source="$BUILD_DIR",target=/build \
        --mount type=bind,source=$(realpath ../openjdk/out),target=/jdk \
        --mount type=bind,source=$(realpath ../fuzzer-target/out),target=/fuzzer-target \
        --env TARGET_CLASS=$TARGET_CLASS \
        --env LD_LIBRARY_PATH=/build \
        $IMAGE_TAG
done
