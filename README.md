# xerces-fuzzer-demo

## Getting started

```
pushd openjdk
./build.sh
popd
```

```
pushd fuzzer-target
./build.sh
popd
```

```
cd jazzer
./build.sh
```

## Fuzz

```
./fuzz.sh
```

## Reproduce the issue

```
./test.sh
```

## Kill all containers

```
./kill.sh
```