package com.example;

import java.nio.file.Files;
import java.io.File;
import java.io.ByteArrayInputStream;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLStreamException;
import java.io.StringReader;



public class JazzerTarget {
    public static void fuzzerInitialize(String[] args) {
    }

    public static void fuzzerTestOneInput(byte[] data) throws Throwable {
        try {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser parser = parserFactory.newSAXParser();
            XMLReader reader = parser.getXMLReader();
            reader.parse(new InputSource(new StringReader(new String(data))));
        }catch (Throwable e){
        }
    }

    public static void main(String[] args) throws Throwable{
        fuzzerInitialize(args);
        for(String arg: args ){
            System.out.println(arg);
            byte[] data = Files.readAllBytes(new File(arg).toPath());
            System.out.println("start");
            fuzzerTestOneInput(data);
            System.out.println("stop");
        }
    }
}