#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

javac -d /build $(find /src -name "*.java" -printf '%p ')

pushd /build
  jar cvf jazzer_target.jar $(find . -name "*.class" -printf '%p ')
popd
