#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

CPU_COUNT=$(grep -c processor /proc/cpuinfo)

cd /build
bash /jdk/configure
make JOBS=$CPU_COUNT images
