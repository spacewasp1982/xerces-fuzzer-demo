#!/usr/bin/env bash

# Fail when commands exit unsuccesfully.
set -o errexit

# Fail when using an undefined variable.
set -o nounset

# Fail if commands fail as part of a pipeline.
set -o pipefail

if [ ! -d jdk-jdk-16-35 ]; then
    wget 'https://github.com/openjdk/jdk/archive/refs/tags/jdk-16+35.zip' -O /tmp/jdk.zip
    unzip -o /tmp/jdk.zip
fi

BUILD_DIR=$(pwd)/out

mkdir -p $BUILD_DIR

IMAGE_TAG="jdk-project"

docker build -t $IMAGE_TAG docker-context

docker run \
    --mount type=bind,source="$BUILD_DIR",target=/build \
    --mount type=bind,source=$(pwd)/jdk-jdk-16-35,target=/jdk \
    --entrypoint="/work/build.sh" \
    $IMAGE_TAG

